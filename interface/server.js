// server.js

// set up ======================================================================
// get all the tools we need
console.log("Start of Server")
var express = require('express'); //Importing Express.js
var app = express(); //Express.js Initialization
var port = process.env.PORT || 8000; //Server Port
var mongoose = require('mongoose'); //Importing Mongoose.js
var passport = require('passport'); //Importing Passport.js
var flash = require('connect-flash'); //Importing tool for diaplaying flash messages
var expressValidator = require('express-validator') //Importing input validation tool
var path = require('path');
var morgan = require('morgan'); //Import HTTP request logging tool
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser'); //Import tool to parse HTTP request body
var session = require('express-session');
var crypto = require('crypto'); //Importing Node.js Crypto library
var configDB = require('./config/database.js'); //Import Database configuration

var GLOBALVARS = require('./config/globalvariables.js')

GLOBALVARS.imgdbpath = GLOBALVARS.fspath + 'imgdbinput/'
GLOBALVARS.tempdir = GLOBALVARS.fspath + 'temp/'
GLOBALVARS.multersave = GLOBALVARS.tempdir + "imgtemp/"
GLOBALVARS.classifiers = GLOBALVARS.fspath + 'classifiers/'
GLOBALVARS.logs = GLOBALVARS.fspath + 'logs/'
GLOBALVARS.snapshotinput = GLOBALVARS.fspath + 'snapshotinput/'
GLOBALVARS.videoinput = GLOBALVARS.fspath + 'videoinput/'
GLOBALVARS.videoprocessed = GLOBALVARS.fspath + 'videoprocessed/'
GLOBALVARS.snapshotprocessed = GLOBALVARS.fspath + 'snapshotprocessed/'
GLOBALVARS.facedetection = GLOBALVARS.fspath + 'facedetection/'
GLOBALVARS.secureKey = crypto.randomBytes(64).toString('hex');

var fs = require('fs-extra');
console.log("Creating directories");
try {
    fs.mkdirSync(GLOBALVARS.fspath)
} catch (e) {}
try {
    fs.mkdirSync(GLOBALVARS.imgdbpath)
} catch (e) {}
try {
    fs.mkdirSync(GLOBALVARS.tempdir)
} catch (e) {}
try {
    fs.mkdirSync(GLOBALVARS.multersave)
} catch (e) {}
try {
    fs.mkdirSync(GLOBALVARS.logs)
} catch (e) {}
try {
    fs.mkdirSync(GLOBALVARS.snapshotinput)
} catch (e) {}
try {
    fs.mkdirSync(GLOBALVARS.snapshotprocessed)
} catch (e) {}
try {
    fs.mkdirSync(GLOBALVARS.facedetection)
} catch (e) {}
try {
    fs.mkdirSync(GLOBALVARS.videoinput)
} catch (e) {}
try {
    fs.mkdirSync(GLOBALVARS.videoprocessed)
} catch (e) {}

console.log("Creating python script child process")
const spawn = require('child_process').spawn;
const facedetectservice = spawn(
    'python2',
    // second argument is array of parameters, e.g.:
    ["../facerecog/dlibfacedetect.py", GLOBALVARS.snapshotinput, GLOBALVARS.snapshotprocessed, GLOBALVARS.facedetection, GLOBALVARS.secureKey, port, GLOBALVARS.videoinput, GLOBALVARS.videoprocessed]
);

facedetectservice.stdout.on('data', (data) => {
    console.log(`stdout from facedetect scrpit : ${data}`);
});

facedetectservice.stderr.on('data', (data) => {
    console.log(`stderr from facedetect scrpit : ${data}`);
});

facedetectservice.on('close', (code) => {
    console.log(` from facedetect scrpit exited with code ${code}`);
});
console.log("Starting file upload engine")
var multer = require('multer');
var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, GLOBALVARS.multersave)
    },
    filename: function(req, file, cb) {
        crypto.pseudoRandomBytes(16, function(err, raw) {
            cb(null, raw.toString('hex') + Date.now() + '.' + file.originalname.toLowerCase().split('.').pop());
        });
    }
});
var upload = multer({
    storage: storage,
    fileFilter: function(req, file, cb) {
        if (!file.originalname.toLowerCase().match(/\.(jpg|png)$/)) {
            return cb(new Error('Only image files(jpg, png) are allowed!'));
        }
        cb(null, true)
    }
}).array('uploadedimages', 32);

// configuration ===============================================================
console.log("Creating database connection")
mongoose.connect(configDB.url); // connect to our database

require('./config/passport')(passport); // pass passport for configuration

// set up our express application
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser.json()); // get information from html forms
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(expressValidator({
    errorFormatter: function(param, msg, value) {
        var namespace = param.split('.'),
            root = namespace.shift(),
            formParam = root;
        while (namespace.length) {
            formParam += '[' + namespace.shift() + ']';
        }
        return {
            param: formParam,
            msg: msg,
            value: value
        };
    }
}));

app.set('view engine', 'ejs'); // set up ejs for templating

// required for passport
app.use(session({
    secret: crypto.randomBytes(48).toString('hex'), // session secret
    resave: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session
console.log("Defining HTTP routes");
// routes ======================================================================
require('./app/routes.js')(app, passport, upload, fs, GLOBALVARS); // load our routes and pass in our app and fully configured passport
console.log("Starting Web server");
// launch ======================================================================
app.listen(port);
console.log('The magic happens on port ' + port);