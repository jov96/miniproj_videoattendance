var Teacher = require('./models/teacher');
var Admin = require('./models/admin');
var Student = require('./models/student');

module.exports = function(app, passport, upload, fs, globalvars) {
    // show the home page (will also have our login links)
    app.get('/', gotoProfile, function(req, res) {
        res.render('index.ejs');
    });

    // PROFILE SECTION =========================
    app.get('/adminprofile', ensureOnlyAdmin, function(req, res) {
        Teacher.
        find().sort({
            name: 1
        }).
        select({
            name: 1,
            username: 1,
            department: 1
        }).
        exec(function(err, teacherdata) {
            Student.
            find().sort({
                teacher: 1,
                name: 1
            }).
            select({
                name: 1,
                username: 1,
                teacher: 1,
                department: 1
            }).
            exec(function(err, studentdata) {
                for (i = 0; i < studentdata.length; i++) {
                    if (studentdata[i].teacher)
                        studentdata[i].teachername = teacherdata.find(function(teacher) {
                            return teacher.username == studentdata[i].teacher;
                        }).name;
                    studentdata[i].imggood = "Good";
                    if (fs.existsSync(globalvars.imgdbpath + studentdata[i].teacher + "/" + studentdata[i].username) &&
                        fs.readdirSync(globalvars.imgdbpath + studentdata[i].teacher + "/" + studentdata[i].username)
                        .length < globalvars.minimages)
                        studentdata[i].imggood = "Not Enough";
                    if (!fs.existsSync(globalvars.imgdbpath + studentdata[i].teacher + "/" + studentdata[i].username) ||
                        !fs.readdirSync(globalvars.imgdbpath + studentdata[i].teacher + "/" + studentdata[i].username).length)
                        studentdata[i].imggood = "No Images";
                }
                res.render('adminprofile.ejs', {
                    greenMessage: req.flash('profileGreenMessage'),
                    redMessage: req.flash('profileRedMessage'),
                    admin: req.user,
                    teacherdata: teacherdata,
                    studentdata: studentdata
                });
            });
        });
    });

    app.get('/teacherprofile', ensureOnlyTeacher, function(req, res) {
        Student.
        find({
            teacher: req.user.username
        }).sort({
            name: 1
        }).
        select({
            name: 1,
            username: 1,
            teacher: 1,
            department: 1
        }).
        exec(function(err, studentdata) {
            for (i = 0; i < studentdata.length; i++) {
                studentdata[i].imggood = "Good";
                if (fs.existsSync(globalvars.imgdbpath + studentdata[i].teacher + "/" + studentdata[i].username) &&
                    fs.readdirSync(globalvars.imgdbpath + studentdata[i].teacher + "/" + studentdata[i].username)
                    .length < globalvars.minimages)
                    studentdata[i].imggood = "Not Enough";
                if (!fs.existsSync(globalvars.imgdbpath + studentdata[i].teacher + "/" + studentdata[i].username) ||
                    !fs.readdirSync(globalvars.imgdbpath + studentdata[i].teacher + "/" + studentdata[i].username).length)
                    studentdata[i].imggood = "No Images";
            }
            res.render('teacherprofile.ejs', {
                greenMessage: req.flash('profileGreenMessage'),
                redMessage: req.flash('profileRedMessage'),
                teacher: req.user,
                studentdata: studentdata
            });
        });
    });

    app.get('/studentprofile', ensureOnlyStudent, function(req, res) {
        res.render('studentprofile.ejs', {
            student: req.user
        });
    });

    app.get('/changeteacher', ensureOnlyAdmin, function(req, res) {
        Student.
        find().sort({
            name: 1
        }).
        select({
            name: 1,
            username: 1,
            department: 1,
            teacher: 1
        }).
        exec(function(err, studentdata) {
            Teacher.
            find().
            select({
                name: 1,
                username: 1
            }).
            exec(function(err, teacherdata) {
                for (i = 0; i < studentdata.length; i++) {
                    if (studentdata[i].teacher)
                        studentdata[i].teachername = teacherdata.find(function(teacher) {
                            return teacher.username == studentdata[i].teacher;
                        }).name;
                }
                res.render('changeteacher.ejs', {
                    greenMessage: req.flash('greenMessage'),
                    redMessage: req.flash('redMessage'),
                    teacherdata: teacherdata,
                    studentdata: studentdata
                });
            });
        });
    });

    app.get('/changeteacher/:studuname/:fromtuname/:teacheruname', ensureOnlyAdmin, function(req, res) {
        var flag = 0;
        Student.where().update({
            username: req.params.studuname
        }, {
            teacher: req.params.teacheruname
        }, function(err, data) {

            if (err)
                flag = 1;
        });
        if (flag)
            req.flash('redMessage', "Error changing!");
        else {
            if (fs.existsSync(globalvars.imgdbpath + req.params.fromtuname + "/" + req.params.studuname)) {
                fs.renameSync(globalvars.imgdbpath + req.params.fromtuname + "/" + req.params.studuname,
                    globalvars.imgdbpath + req.params.teacheruname + "/" + req.params.studuname);
            }
            req.flash('greenMessage', "Successfully changed!");
        }
        res.redirect('/changeteacher');
    });

    // LOGOUT ==============================
    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });

    // LOGIN ===============================
    // show the login form
    app.get('/login', gotoProfile, function(req, res) {
        res.render('login.ejs', {
            redMessage: req.flash('loginRedMessage'),
            greenMessage: req.flash('loginGreenMessage')
        });
    });

    // process the login form
    app.post('/login', passport.authenticate('local-login', {
        successRedirect: '/profile', // redirect to the secure adminprofile section
        failureRedirect: '/login', // redirect back to the login page if there is an error
        failureFlash: true // allow flash messages
    }));

    app.get('/profile', gotoProfile, function(req, res) {
        res.redirect('/login');
    });

    // SIGNUP =================================
    // show the adminsignup form
    app.get('/adminsignup', gotoProfile, function(req, res) { //ADD ensureadmin for production builds
        res.render('adminsignup.ejs', {
            redMessage: req.flash('signupMessage')
        });
    });
    // process the adminsignup form; add isauthenticated for production builds
    app.post('/adminsignup', function(req, res) {
        req.checkBody('name', 'Name is required').notEmpty();
        req.checkBody('username', 'Username is required').notEmpty();
        req.checkBody('password', 'Password is required').notEmpty();
        var errors = req.validationErrors();
        if (errors) {
            for (i in errors) {
                req.flash('signupMessage', errors[i].msg);
            }
            res.redirect('adminsignup');
        } else {
            if (req.body.username)
                req.body.username = req.body.username.toLowerCase(); // Use lower-case username to avoid case-sensitive username matching
            Admin.findOne({
                'username': req.body.username
            }, function(err, admin) {
                // if there are any errors, return the error
                if (err)
                    res.redirect('/adminsignup');
                // check to see if theres already a admin with that username
                if (admin) {
                    req.flash('signupMessage', 'That username is already taken.');
                    res.redirect('/adminsignup');
                } else {
                    Teacher.findOne({
                        'username': req.body.username
                    }, function(err, teacher) {
                        // if there are any errors, return the error
                        if (err)
                            res.redirect('/adminsignup');
                        // check to see if theres already a teacher with that username
                        if (teacher) {
                            req.flash('signupMessage', 'That username is already taken.');
                            res.redirect('/adminsignup');
                        } else {
                            Student.findOne({
                                'username': req.body.username
                            }, function(err, student) {
                                // if there are any errors, return the error
                                if (err)
                                    res.redirect('/adminsignup');
                                // check to see if theres already a student with that username
                                if (teacher) {
                                    req.flash('signupMessage', 'That username is already taken.');
                                    res.redirect('/adminsignup');
                                } else {
                                    // create the user
                                    var newAdmin = new Admin();
                                    newAdmin.name = req.body.name;
                                    newAdmin.username = req.body.username;
                                    newAdmin.password = newAdmin.generateHash(req.body.password);
                                    newAdmin.accType = "A";
                                    newAdmin.save(function(err) {
                                        if (err)
                                            res.redirect('/adminsignup');
                                        if (!req.user)
                                            req.flash('loginGreenMessage', 'Account successfully created.');
                                        else
                                            req.flash('profileGreenMessage', 'Account successfully created.');
                                        res.redirect('/login');
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });

    // show the teachersignup form
    app.get('/teachersignup', ensureOnlyAdmin, function(req, res) {
        res.render('teachersignup.ejs', {
            message: req.flash('signupMessage')
        });
    });
    // process the teachersignup form
    app.post('/teachersignup', ensureOnlyAdmin, function(req, res) {
        if (req.body.username)
            var username = req.body.username.toLowerCase(); // Use lower-case username to avoid case-sensitive username matching
        req.checkBody('name', 'Name is required').notEmpty();
        req.checkBody('username', 'Username is required').notEmpty();
        req.checkBody('password', 'Password is required').notEmpty();
        req.checkBody('department', 'Department is required').notEmpty();
        var errors = req.validationErrors();
        if (errors) {
            for (i in errors) {
                req.flash('signupMessage', errors[i].msg);
            }
            res.redirect('teachersignup');
        } else {
            Teacher.findOne({
                'username': req.body.username
            }, function(err, teacher) {
                // if there are any errors, return the error
                if (err)
                    res.redirect('/teachersignup');
                // check to see if theres already a admin with that username
                if (teacher) {
                    req.flash('signupMessage', 'That username is already taken.');
                    res.redirect('/teachersignup');
                } else {
                    Admin.findOne({
                        'username': req.body.username
                    }, function(err, admin) {
                        // if there are any errors, return the error
                        if (err)
                            res.redirect('/teachersignup');
                        // check to see if theres already a admin with that username
                        if (admin) {
                            req.flash('signupMessage', 'That username is already taken.');
                            res.redirect('/teachersignup');
                        } else {
                            Student.findOne({
                                'username': req.body.username
                            }, function(err, student) {
                                // if there are any errors, return the error
                                if (err)
                                    res.redirect('/teachersignup');
                                // check to see if theres already a admin with that username
                                if (student) {
                                    req.flash('signupMessage', 'That username is already taken.');
                                    res.redirect('/teachersignup');
                                } else {
                                    // create the user
                                    fs.mkdirSync(globalvars.imgdbpath + req.body.username)
                                    var newTeacher = new Teacher();
                                    newTeacher.name = req.body.name;
                                    newTeacher.department = req.body.department;
                                    newTeacher.username = req.body.username;
                                    newTeacher.password = newTeacher.generateHash(req.body.password);
                                    newTeacher.accType = "T";
                                    newTeacher.save(function(err) {
                                        if (err)
                                            res.redirect('/teachersignup');
                                        req.flash('profileGreenMessage', 'Account successfully created.');
                                        res.redirect('/login');
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });

    app.get('/teacherremove/:uname', function(req, res) {
        if (req.user) {
            if (req.user.accType == "A" || (req.user.accType == "T" && req.user.username == req.params.uname)) {
                Student.findOne({
                    teacher: req.params.uname
                }).exec(function(err, data) {
                    if (data) {
                        req.flash('profileRedMessage', "First, remove all students under the teacher, then delete!");
                        res.redirect('/profile');
                    } else {
                        Teacher.remove({
                            'username': req.params.uname
                        }, function(err, data) {
                            if (err) {
                                req.flash('profileRedMessage', 'Error deleting!');
                                res.redirect('/profile');
                            } else {
                                fs.removeSync(globalvars.imgdbpath + req.params.uname);
                                req.flash('profileGreenMessage', 'Successfully deleted!');
                                res.redirect('/profile');
                            }

                        });
                    }
                });
            } else
                res.redirect('/login');
        } else
            res.redirect('/login');
    });

    // show the teachersignup form
    app.get('/studentsignup', ensureTeacherOrAdmin, function(req, res) {
        Teacher.
        find().sort({
            name: 1
        }).
        select({
            name: 1,
            username: 1
        }).
        exec(function(err, teacher) {
            var isTeacher = 0;
            if (req.user.accType == "T") {
                isTeacher = 1;
            }
            res.render('studentsignup.ejs', {
                message: req.flash('signupMessage'),
                teacher: teacher,
                username: req.user.username,
                isTeacher: isTeacher
            });
        });

    });

    // process the studentsignup form
    app.post('/studentsignup', ensureTeacherOrAdmin, function(req, res) {
        upload(req, res, function(err) {
            if (err) {
                req.flash('signupMessage', 'Upload only images!');
                res.redirect('/studentsignup');
            } else {
                if (req.body.username)
                    var username = req.body.username.toLowerCase(); // Use lower-case username to avoid case-sensitive username matching
                req.checkBody('name', 'Name is required').notEmpty();
                req.checkBody('username', 'Username is required').notEmpty();
                req.checkBody('password', 'Password is required').notEmpty();
                req.checkBody('department', 'Department is required').notEmpty();
                var errors = req.validationErrors();
                if (errors) {
                    for (i in errors) {
                        req.flash('signupMessage', errors[i].msg);
                    }
                    res.redirect('studentsignup');
                } else {
                    Teacher.findOne({
                        'username': req.body.username
                    }, function(err, teacher) {
                        // if there are any errors, return the error
                        if (err)
                            res.redirect('/studentsignup');
                        // check to see if theres already a admin with that username
                        if (teacher) {
                            req.flash('signupMessage', 'That username is already taken.');
                            res.redirect('/studentsignup');
                        } else {
                            Admin.findOne({
                                'username': req.body.username
                            }, function(err, admin) {
                                // if there are any errors, return the error
                                if (err)
                                    res.redirect('/studentsignup');
                                // check to see if theres already a admin with that username
                                if (admin) {
                                    req.flash('signupMessage', 'That username is already taken.');
                                    res.redirect('/studentsignup');
                                } else {
                                    Student.findOne({
                                        'username': req.body.username
                                    }, function(err, student) {
                                        // if there are any errors, return the error
                                        if (err)
                                            res.redirect('/studentsignup');
                                        // check to see if theres already a admin with that username
                                        if (student) {
                                            req.flash('signupMessage', 'That username is already taken.');
                                            res.redirect('/studentsignup');
                                        } else {
                                            if (req.user.accType == "T") {
                                                req.body.teacher = req.user.username;
                                            }
                                            fs.renameSync(globalvars.tempdir + 'imgtemp/', globalvars.imgdbpath + req.body.teacher + "/" + req.body.username);
                                            fs.mkdirSync(globalvars.tempdir + 'imgtemp/')
                                                // create the user
                                            var newStudent = new Student();
                                            newStudent.name = req.body.name;
                                            newStudent.department = req.body.department;
                                            newStudent.username = req.body.username;
                                            newStudent.password = newStudent.generateHash(req.body.password);
                                            newStudent.accType = "S";
                                            newStudent.teacher = req.body.teacher;
                                            newStudent.attendance = [];
                                            newStudent.save(function(err) {
                                                if (err) {
                                                    req.flash('signupMessage', 'DB Error!');
                                                    res.redirect('/studentsignup');
                                                }
                                                req.flash('profileGreenMessage', 'Account successfully created.');
                                                res.redirect('/login');
                                            });

                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            }
        })
    });

    app.get('/viewattendance/:uname', function(req, res) {
        if (req.user) {
            Student.findOne({
                username: req.params.uname
            }).exec(function(err, stud) {
                if (err)
                    res.redirect('/profile');
                if (req.user.accType == "A" || (req.user.accType == "T" && stud.teacher == req.user.username) ||
                    (req.user.accType == "S" && req.user.username == req.params.uname)) {
                    res.render('viewattendance.ejs', {
                        user: req.user,
                        stud: stud,
                        greenMessage: req.flash('greenMessage'),
                        redMessage: req.flash('redMessage')
                    });
                } else
                    res.redirect('/profile');
            });
        } else
            res.redirect('/');
    });

    app.get('/changeattendance/:uname/:yyyy/:mm/:dd', function(req, res) {
        if (req.user) {
            var d = new Date(req.params.mm + " " + req.params.dd + " " + req.params.yyyy);
            Student.findOne({
                username: req.params.uname
            }).select({
                name: 1,
                username: 1,
                attendance: {
                    $elemMatch: {
                        date: d
                    }
                }
            }).exec(function(err, stud) {
                if (err)
                    res.redirect('/viewattendance/' + req.params.uname);
                if (req.user.accType == "A" || (req.user.accType == "T" && stud.teacher == req.user.username)) {
                    if (!stud.attendance[0]) {
                        res.redirect('/profile');
                    }
                    res.render('changeattendance.ejs', {
                        stud: stud
                    });
                }
            });
        } else
            res.redirect('/');
    });

    app.post('/changeattendance/:uname/:yyyy/:mm/:dd', ensureTeacherOrAdmin, function(req, res) {
        var d = new Date(req.params.mm + " " + req.params.dd + " " + req.params.yyyy);
        aa = [];
        for (i = 0; i < globalvars.periods; i++) {
            aa[i] = {
                period: i + 1,
                absent: eval("req.body.period" + (i + 1))
            };
        }
        Student.findOne({
            username: req.params.uname,
        }).update({
            'attendance.date': d
        }, {
            '$set': {
                'attendance.$.a': aa
            }
        }, function(err, data) {
            if (!err) {
                req.flash('greenMessage', "Update Successful");
                res.redirect('/viewattendance/' + req.params.uname);
            } else {
                req.flash('redMessage', "Update Unsuccessful");
                res.redirect('/profile');
            }
        });
    });

    app.get('/addattendance/:uname/', ensureTeacherOrAdmin, function(req, res) {
        if (req.user) {
            Student.findOne({
                username: req.params.uname
            }).select({
                name: 1,
                username: 1
            }).exec(function(err, stud) {
                if (err)
                    res.redirect('/viewattendance/' + req.params.uname);
                if (req.user.accType == "A" || (req.user.accType == "T" && stud.teacher == req.user.username)) {
                    res.render('addattendance.ejs', {
                        stud: stud,
                        periods: globalvars.periods
                    });
                }
            });
        } else
            res.redirect('/');
    });

    app.post('/addattendance/:uname', ensureTeacherOrAdmin, function(req, res) {
        var d = new Date(req.body.date);
        d = new Date(d.toDateString());
        aa = [];
        for (i = 0; i < globalvars.periods; i++) {
            aa[i] = {
                period: i + 1,
                absent: eval("req.body.period" + (i + 1))
            };
        }
        att = {
            date: d,
            a: aa
        }
        Student.findOne({
            username: req.params.uname,
            attendance: {
                $elemMatch: {
                    date: d
                }
            }
        }).exec(function(err, data) {
            if (data) {
                req.flash('redMessage', "Record Exists! Click on Change at corresponding date.");
                res.redirect('/viewattendance/' + req.params.uname);
            } else {
                Student.findOne({
                    username: req.params.uname
                }).exec(function(err, stud) {
                    stud.attendance.push(att);
                    stud.save();
                    req.flash('greenMessage', "Successfully Added");
                    res.redirect('/viewattendance/' + req.params.uname);
                });
            }
        });
    });

    app.post('/addattendancesingle/', function(req, res) {
        if (req.body.secureKey == globalvars.secureKey) {
            confidence = parseFloat(req.body.confidence);
            if (confidence > globalvars.minConfidence) {
                parsedname = req.body.imagename.split('-');
                teacher = parsedname[0];
                datetime = new Date(parsedname[1], parsedname[2] - 1, parsedname[3], parsedname[4], parsedname[5]);
                student = req.body.student;
                var d = new Date(parsedname[2] + " " + parsedname[3] + " " + parsedname[1]);
                ds = d.toDateString()
                periodtime = []
                for (i of globalvars.periodtime) {
                    periodtime.push([new Date(ds + ' ' + i[0]), new Date(ds + ' ' + i[1])])
                }
                flag = 0;
                for (let [index, val] of periodtime.entries()) {
                    if (val[0] <= datetime && datetime <= val[1]) {
                        flag = 1;
                        Student.findOne({
                            username: student,
                        }).exec(function(err, stud) {
                            if (err)
                                res.end("Invalid student username")
                            if (stud.teacher == teacher) {
                                Student.findOne({
                                    username: student,
                                    attendance: {
                                        $elemMatch: {
                                            date: d
                                        }
                                    }
                                }).exec(function(err, data) {
                                    if (data) {
                                        for (k of data.attendance) {
                                            if (k.date.toISOString() == d.toISOString()) {
                                                k.a[index].absent = false;
                                                data.save();
                                                res.end("Modified Record")
                                            }
                                        }
                                    } else {
                                        Student.findOne({
                                            username: student
                                        }).exec(function(err, stud1) {
                                            aa = [];
                                            for (i = 0; i < globalvars.periods; i++) {
                                                aa[i] = {
                                                    period: i + 1,
                                                    absent: true
                                                };
                                            }
                                            aa[index] = {
                                                period: index + 1,
                                                absent: false
                                            }
                                            att = {
                                                date: d,
                                                a: aa
                                            }
                                            stud1.attendance.push(att); //TODO
                                            stud1.save();
                                            res.end('Added record');
                                        });
                                    }
                                });
                            } else
                                res.end("Data mismatch (Teacher Name doesnt match Student's Class Teacher Name)")
                                //stud.attendance.push(att);
                                //stud.save();
                                //req.flash('greenMessage', "Successfully Added");
                                //res.redirect('/viewattendance/' + req.params.uname);
                        });
                    }
                }
                if (flag == 0)
                    res.end('Image not shot during any valid periods')
            } else
                res.end("Not enough confidence");
        } else
            res.end("Wrong authentication token, real : " + globalvars.secureKey + ", received : " + req.body.secureKey);
    });

    app.get('/viewimages/:teacher', ensureTeacherOrAdmin, function(req, res) {
        require('fs').readdir(globalvars.snapshotprocessed, function(err, files) {
            if (err) {
                res.end("Error");
            } else {
                var a = files.filter(function(word) {
                    return word.startsWith(req.params.teacher);
                })
                res.render('viewimages.ejs', {
                    a: a
                })
            }
        });
    });

    app.get('/getimage/:name', ensureTeacherOrAdmin, function(req, res, next) {
        var options = {
            root: './',
            dotfiles: 'deny',
            headers: {
                'x-timestamp': Date.now(),
                'x-sent': true
            }
        };
        var fileName = globalvars.snapshotprocessed + req.params.name;
        res.sendFile(fileName, options, function(err) {
            if (err) {
                next(err);
            }
        });
    });

    app.get('/viewlog', ensureOnlyAdmin, function(req, res) {
        if (fs.existsSync(globalvars.logs + 'classifier.log')) {
            c = fs.readFileSync(globalvars.logs + 'classifier.log').toString().replace(/\n/g, '<br/>');
            if (fs.existsSync(globalvars.logs + 'train_classifier.log')) {
                tc = fs.readFileSync(globalvars.logs + 'train_classifier.log').toString().replace(/\n/g, '<br/>');
                res.render('viewlog.ejs', {
                    tc: tc,
                    c: c
                })
            } else
                res.render('viewlog.ejs', {
                    tc: "",
                    c: c
                })
        }
    });
    app.get('/studentremove/:uname', ensureOnlyAdmin, function(req, res) {
        if (req.user) {
            var flag = 0;
            Student.
            findOne({
                username: req.params.uname
            }).
            select({
                teacher: 1
            }).
            exec(function(err, stud) {
                Student.remove({
                    'username': req.params.uname
                }, function(err, data) {
                    if (err)
                        var flag = 1;
                });
                if (flag)
                    req.flash('profileRedMessage', 'Error deleting!');
                else
                    req.flash('profileGreenMessage', 'Successfully deleted!');
                res.redirect('/profile');
            });
        } else
            res.redirect('/login');
    });

    app.get('/addimages/:uname', ensureTeacherOrAdmin, function(req, res) {
        Student.findOne({
            username: req.params.uname
        }).select({
            name: 1,
            username: 1,
            teacher: 1
        }).exec(function(err, data) {
            res.render('addimages.ejs', {
                studdata: data,
                message: req.flash('redMessage')
            })
        });
    });

    app.post('/addimages/:uname', ensureTeacherOrAdmin, function(req, res) {
        upload(req, res, function(err) {
            var fl = 0;
            if (err) {
                req.flash('redMessage', 'Upload only images!');
                res.redirect('/addimages/' + req.params.uname);
            } else {
                fs.removeSync(globalvars.imgdbpath + req.body.teacher + "/" + req.params.uname);
                fs.renameSync(globalvars.tempdir + 'imgtemp/', globalvars.imgdbpath + req.body.teacher + "/" + req.params.uname);
                fs.mkdirSync(globalvars.tempdir + 'imgtemp/')
                req.flash('profileGreenMessage', "Successfully added images");
                res.redirect('/profile');
            }
        });
    });

    app.get('/rebuildclassifiers', ensureOnlyAdmin, function(req, res) {
        var python = require('child_process').spawn(
            'python2',
            // second argument is array of parameters, e.g.:
            ["../facerecog/train_classifier.py", globalvars.imgdbpath, globalvars.classifiers]
        );
        req.flash('profileGreenMessage', 'Training Started, Check log for status!');
        res.redirect('/profile');
    });
};

function ensureOnlyAdmin(req, res, next) { //Only admins
    if (req.user)
        if (req.user.accType == "A") {
            return next();
        }
    res.redirect('/login')
}

function ensureOnlyTeacher(req, res, next) { //Only teachers
    if (req.user)
        if (req.user.accType == "T") {
            return next();
        }
    res.redirect('/login')
}

function ensureTeacherOrAdmin(req, res, next) { //Only teachers and admin
    if (req.user)
        if (req.user.accType == "T" || req.user.accType == "A") {
            return next();
        }
    res.redirect('/login')
}

function ensureOnlyStudent(req, res, next) { //Only students
    if (req.user)
        if (req.user.accType == "S") {
            return next();
        }
    res.redirect('/login')
}

function gotoProfile(req, res, next) {
    if (req.user) {
        //console.log(req.user.constructor.modelName)
        if (req.user.accType == "A") {
            res.redirect('/adminprofile');
        } else if (req.user.accType == "T") {
            res.redirect('/teacherprofile');
        } else if (req.user.accType == "S") {
            res.redirect('/studentprofile');
        }
    } else
        return next();
}