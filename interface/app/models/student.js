// load the things we need
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

// define the schema for our user module
var studentSchema = mongoose.Schema({
    accType: String,
    name: String,
    username: String,
    password: String,
    department: String,
    teacher: String,
    attendance: [{
        date: Date,
        a: [{
            period: Number,
            absent: Boolean
        }]
    }]
});

// generating a hash
studentSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
studentSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

// create the model for users and expose it to our app
module.exports = mongoose.model('Student', studentSchema);