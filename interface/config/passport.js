// load all the things we need
var LocalStrategy = require('passport-local').Strategy;
// load up the admin model
var Admin = require('../app/models/admin');
var Teacher = require('../app/models/teacher');
var Student = require('../app/models/student');

module.exports = function(passport) {

    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session
    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        if (id.charAt(0) == "A") {
            Admin.findById(id.substr(1), function(err, admin) {
                done(err, admin);
            });
        } else if (id.charAt(0) == "T") {
            Teacher.findById(id.substr(1), function(err, teacher) {
                done(err, teacher);
            });
        } else if (id.charAt(0) == "S") {
            Student.findById(id.substr(1), function(err, student) {
                done(err, student);
            });
        }
    });

    // used to serialize the admin for the session
    passport.serializeUser(function(user, done) {
        done(null, user.accType + user.id);
    });

    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
    passport.use('local-login', new LocalStrategy({
            // by default, local strategy uses username and password, we will override with username
            usernameField: 'username',
            passwordField: 'password',
            passReqToCallback: true // allows us to pass in the req from our route (lets us check if a admin is logged in or not)
        },
        function(req, username, password, done) {
            if (username)
                username = username.toLowerCase(); // Use lower-case e-mails to avoid case-sensitive e-mail matching

            // asynchronous
            process.nextTick(function() {
                Admin.findOne({
                    'username': username
                }, function(err, admin) {
                    // if there are any errors, return the error
                    if (err)
                        return done(err);
                    if (admin) {
                        if (admin.validPassword(password)) {
                            return done(null, admin);
                        } else {
                            return done(null, false, req.flash('loginRedMessage', 'Oops! Wrong password.'));
                        }
                    }
                    Teacher.findOne({
                        'username': username
                    }, function(err, teacher) {
                        // if there are any errors, return the error
                        if (err)
                            return done(err);
                        if (teacher) {
                            if (teacher.validPassword(password)) {
                                return done(null, teacher);
                            } else {
                                return done(null, false, req.flash('loginRedMessage', 'Oops! Wrong password.'));
                            }
                        }
                        Student.findOne({
                            'username': username
                        }, function(err, student) {
                            // if there are any errors, return the error
                            if (err)
                                return done(err);
                            if (student) {
                                if (student.validPassword(password)) {
                                    return done(null, student);
                                } else {
                                    return done(null, false, req.flash('loginRedMessage', 'Oops! Wrong password.'));
                                }
                            }
                            // if no admin is found, return the message
                            if (!teacher && !admin && !student)
                                return done(null, false, req.flash('loginRedMessage', 'No user found.'));
                        });
                    });

                });
            });

        }));
};