// config/globalvariables.js
module.exports = { //List of global variables, set to Admin's choice
    periods: 7, //Number of periods
    fspath: './temp/', //Writable path for saving and processing images, log files, etc
    minimages: 4, //Minimum number of images required per student
    periodtime: [ //Start and end time of 'periods' number of periods in 24 hour format
        ["08:30", "09:30"],
        ["09:30", "10:30"],
        ["10:45", "11:40"],
        ["11:40", "12:35"],
        ["13:35", "14:30"],
        ["14:40", "15:35"],
        ["15:35", "16:30"]
    ],
    minConfidence: 0.60
};