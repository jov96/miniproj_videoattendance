#!/usr/bin/python

import sys
import dlib
import cv2
import logging
import os
from datetime import datetime
from random import randint
import time
sys.path.append('../facerecog')
import classifier

logging.basicConfig(filename='temp/logs/dlibfacedetect.log', format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p',level=logging.DEBUG)
logging.info('START of dlibfacedetect')

if len(sys.argv) == 8:
    videoinpath = sys.argv[6]
    videooutpath = sys.argv[7]
    inpath = sys.argv[1]
    snapoutpath = sys.argv[2]
    faceoutpath = sys.argv[3]
    secureKey = sys.argv[4]
    servPort = sys.argv[5]
    detector = dlib.get_frontal_face_detector()
    while 1:
        logging.info("Start of iteration")
        videospath = [os.path.join(videoinpath, f) for f in os.listdir(videoinpath)]
        for videopath in videospath:
            logging.info("Taking snapshots from : " + videopath)
            vidcap = cv2.VideoCapture(videopath)
            if(vidcap.isOpened()):
                frames = vidcap.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT)
                frames_minute = frames / 60
                start_frame = randint(frames_minute * 5 , frames_minute * 6)
                vidcap.set(cv2.cv.CV_CAP_PROP_POS_FRAMES, start_frame)
                success,image = vidcap.read()
                if(success):
                    cv2.imwrite(os.path.join(inpath, os.path.basename(videopath).split('.')[0] + "-0.jpg"), image)
                else:
                    logging.info("Error fetching start frame")
                mid_frame = randint(frames_minute * 30 , frames_minute * 31)
                vidcap.set(cv2.cv.CV_CAP_PROP_POS_FRAMES, mid_frame)
                success,image = vidcap.read()
                if(success):
                    cv2.imwrite(os.path.join(inpath, os.path.basename(videopath).split('.')[0] + "-1.jpg"), image)
                else:
                    logging.info("Error fetching mid frame")
                last_frame = randint(frames_minute * 55 , frame_minute * 56)
                vidcap.set(cv2.cv.CV_CAP_PROP_POS_FRAMES, last_frame)
                success,image = vidcap.read()
                if(success):
                    cv2.imwrite(os.path.join(inpath, os.path.basename(videopath).split('.')[0] + "-2.jpg"), image)
                else:
                    logging.info("Error fetching last frame")
            os.rename(videopath, os.path.join(videooutpath, os.path.basename(videopath)))
        imagespath = [os.path.join(inpath, f) for f in os.listdir(inpath)]
        for imagepath in imagespath:
            logging.info("Detecting faces in : " + imagepath)
            #counter = 0
            img = cv2.imread(imagepath)
            dets = detector(img, 1)
            logging.info("Number of faces detected: {}".format(len(dets)))
            for i, d in enumerate(dets):
                logging.info("Detection {}: Left: {} Top: {} Right: {} Bottom: {}".format(
                    i, d.left(), d.top(), d.right(), d.bottom()))
                l = d.left()
                r = d.right()
                t = d.top()
                b = d.bottom()
                crop_img = img[t:b, l:r]
                filename = os.path.basename(imagepath).split('.')[0]
                classifier.infer(filename, crop_img, secureKey, servPort)
                #img_save_path = os.path.join(sys.argv[3], os.path.basename(imagepath).split('.')[0] + "-" + str(counter) + ".jpg")
                #print img_save_path
                #cv2.imwrite(img_save_path, crop_img)
                #counter = counter + 1
            os.rename(imagepath, os.path.join(sys.argv[2], os.path.basename(imagepath)))
        time.sleep(10)