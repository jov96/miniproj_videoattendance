#!/usr/bin/env python2
#
# Example to classify faces.
# Brandon Amos
# 2015/10/11
#
# Copyright 2015-2016 Carnegie Mellon University
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import time

start = time.time()

import cv2
import os
import pickle

from operator import itemgetter

import numpy as np
np.set_printoptions(precision=2)
import pandas as pd

import openface

from sklearn.pipeline import Pipeline
from sklearn.lda import LDA
from sklearn.preprocessing import LabelEncoder
from sklearn.svm import SVC
from sklearn.grid_search import GridSearchCV
from sklearn.mixture import GMM
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB

import requests
import logging
logging.basicConfig(filename='temp/logs/classifier.log', format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p',level=logging.DEBUG)
logging.info('START of classifier')

fileDir = '/root/openface/demos'

modelDir = os.path.join(fileDir, '..', 'models')
dlibModelDir = os.path.join(modelDir, 'dlib')
openfaceModelDir = os.path.join(modelDir, 'openface')

#Initialize with defaults
align = openface.AlignDlib('/root/openface/models/dlib/shape_predictor_68_face_landmarks.dat')
net = openface.TorchNeuralNet('/root/openface/models/openface/nn4.small2.v1.t7', imgDim=96, cuda=False)
logging.info('Neural network ready')

def getRep(bgrImg, filenm, multiple=False):
    start = time.time()
    if bgrImg is None:
        raise Exception("Unable to load image: {}".format(filenm))

    rgbImg = cv2.cvtColor(bgrImg, cv2.COLOR_BGR2RGB)

#    if args.verbose:
#        print("  + Original size: {}".format(rgbImg.shape))
#    if args.verbose:
#        print("Loading the image took {} seconds.".format(time.time() - start))

    start = time.time()

    if multiple:
        bbs = align.getAllFaceBoundingBoxes(rgbImg)
    else:
        bb1 = align.getLargestFaceBoundingBox(rgbImg)
        bbs = [bb1]
    if len(bbs) == 0 or (not multiple and bb1 is None):
        #raise Exception("Unable to find a face: {}".format(filenm))
        logging.info("Unable to find a face")
        return []
#    if args.verbose:
#        print("Face detection took {} seconds.".format(time.time() - start))

    reps = []
    for bb in bbs:
        start = time.time()
        alignedFace = align.align(
            96,
            rgbImg,
            bb,
            landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE)
        if alignedFace is None:
            logging.info("Unable to align image")
            return []
            #raise Exception("Unable to align image: {}".format(filenm))
#        if args.verbose:
#            print("Alignment took {} seconds.".format(time.time() - start))
#            print("This bbox is centered at {}, {}".format(bb.center().x, bb.center().y))

        start = time.time()
        rep = net.forward(alignedFace)
#        if args.verbose:
#            print("Neural network forward pass took {} seconds.".format(
#                time.time() - start))
        reps.append((bb.center().x, rep))
    sreps = sorted(reps, key=lambda x: x[0])
    return sreps

def infer(arg_img, img, secureKey, servPort, multiple=False):
    logging.info('infer() called, img name : ' + arg_img)
    clsfrmodel = 'temp/classifiers/' + arg_img.split('-')[0] + '/classifier.pkl'
    with open(clsfrmodel, 'r') as f:
        (le, clf) = pickle.load(f)
    logging.info('Procesing image : ' + arg_img)
    reps = getRep(img, arg_img, multiple)
    for r in reps:
        rep = r[1].reshape(1, -1)
        bbx = r[0]
        start = time.time()
        predictions = clf.predict_proba(rep).ravel()
        maxI = np.argmax(predictions)
        person = le.inverse_transform(maxI)
        confidence = predictions[maxI]
#        if args.verbose:
#            print("Prediction took {} seconds.".format(time.time() - start))
        if not multiple:
            #This is what we want
            logging.info('Predict ' + person + ' with ' + str(confidence) + ' confidence.')
            addattendanceURL = 'http://localhost:' + servPort + '/addattendancesingle'
            r = requests.post(addattendanceURL, data = {'student': person, 'confidence': str(confidence), 'imagename' : arg_img, 'secureKey': secureKey})
            logging.info('POST Resonse : ' + r.text)
