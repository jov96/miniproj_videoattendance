import sys
import os
import shutil
import logging
logging.basicConfig(filename='temp/logs/train_classifier.log', format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p',level=logging.DEBUG)
logging.info('START of train_classifier')

if len(sys.argv) == 3:
  if os.path.isdir(sys.argv[2]):
    shutil.rmtree(sys.argv[2])
  if os.path.isdir('temp/imgdboutput/'):
    shutil.rmtree('temp/imgdboutput/')
  os.chdir('../../openface/')
  inpath = os.path.join('../miniproj_videoattendance/interface/', sys.argv[1])
  outpath = '../miniproj_videoattendance/interface/temp/imgdboutput/'
  classifieroutpath = os.path.join('../miniproj_videoattendance/interface/', sys.argv[2])

  logging.info("Detecting faces and aligning")
  imagespath = [os.path.join(inpath, f) for f in os.listdir(inpath)]
  for imagepath in imagespath:
    logging.info(imagepath)
    os.system('python2 ./util/align-dlib.py ' + imagepath + ' align outerEyesAndNose ' + os.path.join(outpath,  imagepath.split('/').pop()) + ' --size 96')

  logging.info("Creating features")
  outimagespath = [os.path.join(outpath, f) for f in os.listdir(outpath)]
  for outimagepath in outimagespath:
    logging.info(outimagepath)
    os.system('./batch-represent/main.lua -outDir ' + os.path.join(classifieroutpath, outimagepath.split('/').pop()) +' -data ' + outimagepath)

  logging.info("Training classifier")
  classifierspath = [os.path.join(classifieroutpath, f) for f in os.listdir(classifieroutpath)]
  for classifierpath in classifierspath:
    logging.info(classifierspath)
    os.system('./demos/classifier.py train ' + classifierpath)
  logging.info("Finished training, removing temporary files")
  if os.path.isdir('temp/imgdboutput/'):
    shutil.rmtree('temp/imgdboutput/')
  logging.info("DONE")
